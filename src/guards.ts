import { Hoge } from './Hoge';

/**
 * 引数 x が number 型であれば true、そうでなければ false を返却。
 */
export const isNumber = (x: unknown): x is number => typeof x === "number";


/**
 * 引数 x が Hoge 型であれば true、そうでなければ false を返却。
 * @class Hoge
 */
export const isHoge = (arg: unknown): arg is Hoge => arg instanceof Hoge;


/**
 * 引数 arg が Fuga インターフェースを実装していれば true、
 * そうでなければ false を返却。
 * @see Fuga.d.ts
 */
export const isFuga = (arg: unknown): arg is Fuga => {
  return Boolean(arg)
         && typeof arg === 'object'
         && typeof (arg as Partial<Fuga>).x === 'number'
         && typeof (arg as Partial<Fuga>).y === 'number';
};


/**
 * 引数 x が number 型でなければ TypeError を送出する
 */
export function assertIsNumber(x: unknown): asserts x is number {
  if (typeof x !== 'number') {
    throw new TypeError();
  }
};
