import { isNumber, isHoge, isFuga, assertIsNumber } from "./guards";
import { Hoge } from "./Hoge";

(() => {
  console.log(isNumber(1)); // true

  const hoge = new Hoge();
  console.log(isHoge(hoge)); // true

  const fuga: Fuga = { x: 1, y: 10 };
  console.log(isFuga(fuga)); // true

  assertIsNumber('hoge'); // TypeError
})();
