# ユーザー定義型ガード


## プリミティブ型
`typeof`[^typeof] 演算子を利用してチェックできる  
```ts
/**
 * 引数 x が number 型であれば true、そうでなければ false を返却。
 */
const isNumber = (x: unknown): x is number => typeof x === "number";
```


## クラス
`instanceof`[^instanceof] 演算子を利用してチェックできる  
```ts
/**
 * 引数 x が Hoge 型であれば true、そうでなければ false を返却。
 */
const isHoge = (arg: unknown): arg is Hoge => arg instanceof Hoge;
```


## タイプ・インターフェース
タイプやインターフェースを実装したオブジェクトであるかの確認には `instanceof` 演算子は使えない。  
`type` や `interface` は TypeScript の独自実装であり、トランスパイル後の JavaScript には型情報として残らない。  
その為、以下の条件を全て満たすかをチェックする。
- Truthy[^Truthy] であるか
- object 型であるか
- プロパティが定義通りの型か
```ts
/**
 * x と y をプロパティとして持つ Fuga 型
 */
type Fuga = {
  x: number;
  y: number;
};

/**
 * arg が Fuga を実装していれば true、
 * そうでなければ false を返却する。
 */
const isFuga = (arg: unknown): arg is Fuga => {
  return Boolean(arg)
         && typeof arg === 'object'
         && typeof (arg as Partial<Fuga>).x === 'number'
         && typeof (arg as Partial<Fuga>).y === 'number';
};
```
`in`[^in] 演算子を用いればプロパティの存在をチェックできるが、
```ts
'x' in (arg as Partial<Hoge>)
```
の様な記述は不要。  
`arg` に `x` や `y` プロパティが存在しなければ
```ts
typeof (arg as Partial<Hoge>).x === undefined
typeof (arg as Partial<Hoge>).y === undefined
```
であり、`=== 'number'` の条件を満たさない為。  


## asserts を利用した型チェック関数
`is` のみで定義した型ガードは戻り値が `true` であれば通過したのに対し、  
`asserts` で定義した場合は **例外がスローされなければ通過** という仕組みになっている。
```ts
/**
 * 引数 x が number 型でなければ TypeError を送出する
 */
function assertIsNumber(x: unknown): asserts x is number {
  if (typeof x !== 'number') {
    throw new TypeError();
  }
}
```
`asserts` 関数はアロー関数で宣言すると利用側でエラー `(ts2775)` になるため、`function` で定義する。
```ts: アロー関数を用いた場合 (エラー)
const assertIsNumber = (x: unknown): asserts x is number => {
  if (typeof x !== 'number') {
    throw new TypeError();
  }
};

assertIsNumber('hoge');  // エラー (ts2775)
```


## 参考
- [TypeScript: interfaceにはinstanceofが使えないので、ユーザ定義タイプガードで対応する - Qiita](https://qiita.com/suin/items/0ce77f31cbaa14031288)
- [TypeScript 3.7の\`asserts x is T\`型はどのように危険なのか - Qiita](https://qiita.com/uhyo/items/b8d2ea6fbf6214fc4194)
- [【TypeScript】Utility Typesをまとめて理解する - Qiita](https://qiita.com/k-penguin-sato/items/e2791d7a57e96f6144e5)
- [TypeScript 3.7 の Assertion Functions でコンパイルエラー - Qiita](https://qiita.com/arx8/items/a87fe4bb4bf9be89a146)

## 脚注
[^typeof]: [typeof - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Operators/typeof)  
[^instanceof]: [instanceof - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Operators/instanceof)  
[^in]: [in - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Operators/in)  
[^Truthy]: [Truthy - MDN Web Docs 用語集: ウェブ関連用語の定義 | MDN](https://developer.mozilla.org/ja/docs/Glossary/Truthy)  
